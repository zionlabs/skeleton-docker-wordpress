include .env

.PHONY: all $(MAKECMDGOALS)

## clear:		supprime les conteneurs et les volumes
clear:
	sudo rm -rf logs/*
	docker-compose -p ${APP_NAME} kill
	docker-compose -p ${APP_NAME} down --volumes --remove-orphans

## delete-bdd:	supprime les données de la base persistées sur le disque
delete-bdd:
	sudo rm -rf donnees-bdd/

## help:		liste toutes les commandes disponibles du makefile
help:	Makefile
	@sed -n 's/^##//p' $<

## start:		démarre tout le truc
start:
	docker-compose -p ${APP_NAME} up -d
	@echo "****************************"
	@echo "site accessible sur http://localhost:${WEB_PORT}"

## stop:		arrêtes les conteneurs
stop:
	docker-compose stop

## reset:		supprime les conteneurs et relance
reset: clear start

## restart:	redémarre les conteneurs
restart: stop start