# skeleton-docker-wordpress
### Exemple de mise en place d'un docker pour Wordpress

## Pré-requis
Avoir déjà installé:
- docker, docker-compose
- make


# TLDR ; Démarrage rapide

```
cp .env.exemple .env
make start
```
L'application sera accessible sur http://localhost

## Variables d'environnement
- vous pouvez personnaliser le fichier .env
- ne JAMAIS commiter le fichier .env (mauvaise pratique)
- le fichier .env fonctionne automatiquement avec la commande "docker-compose up" mais ne fonctionne pas avec "docker stack deploy"


# Commandes (à exécuter depuis la racine du projet)
Lancement (en mode détaché => récupérer le prompt)
```
make start
```

Voir toutes les commandes du makefile
```
make help
```

Les données sont préservées dans le dossier donnees-bdd.
En cas de changement de mot passe, supprimer le dossier /donnees-bdd/mysql.